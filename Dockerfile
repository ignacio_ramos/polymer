#Imagen Base
FROM node:latest

#Directorio de la aplicación enn el contenedor
WORKDIR /app

#Copiado de archivo
ADD . /app

#Dependencias
#RUN npm install

#Puerto de exposición
EXPOSE 3000

#Comando
CMD ["npm","start"]
